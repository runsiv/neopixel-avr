from tkinter import *
from serial_handler import UART
global  uart
uart = UART()


if __name__=="__main__":
    gui = Tk()
    gui.title("LED Controller")
    gui.geometry("320x280")

    button_on = Button(gui, text="Turn On")
    button_on.grid(row=1, column=2)

    button_off = Button(gui, text="Turn Off")
    button_off.grid(row=1, column=3)

    button_invert = Button(gui, text="Invert LED")
    button_invert.grid(row=2, column=4)

    button_grow = Button(gui, text="Grow")
    button_grow.grid(row=2, column=2)

    button_shrink = Button(gui, text="Shrink")
    button_shrink.grid(row=2, column=3)


    gui.mainloop()


