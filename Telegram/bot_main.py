import telegram
from telegram import Update, ForceReply
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
import logging
import serial 
import serial.tools.list_ports
from serial_handler import UART


logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.INFO)

bot = telegram.Bot(token = "2055344400:AAG9-5zc1TSkxfjHLzOs7C4kuV3o9ArO4qw")
updater = Updater(token='2055344400:AAG9-5zc1TSkxfjHLzOs7C4kuV3o9ArO4qw', use_context=True)
#start_handler = CommandHandler('start', start)
logger = logging.getLogger(__name__)

global  uart
uart = UART()

def start(update: Update, context: CallbackContext) -> None:
    context.bot.send_message(chat_id=update.effective_chat.id, text="Distance LED show started!")
""" 
def start_measurement(update: Update, context: CallbackContext) -> None:
     update.message.reply_text("Measurements is now running ")

def stop_measurement(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Measurements is now stopped ")
"""
def turn_on_light(update: Update, context: CallbackContext) -> None:
    var = "ON\r\n"
    uart.write(var)
    update.message.reply_text("Light'S on")
    # if uart.ser.isOpen():
    #     uart.write(var)
    # else: 
    #         uart.ser.open()
    #         uart.write(var)

    # text = uart.readline()
    # print(text)
    # update.message.reply_text(text)
    
     
def turn_off_light(update: Update, context: CallbackContext) -> None:
    var = "OFF\r\n"
    uart.write(var)
    update.message.reply_text("Light's off")

def change_colour(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Color changed to: ")

def invert(update: Update, context: CallbackContext) -> None:
    update.message.reply_text("Light inverted")

def error(update: Update, context: CallbackContext) -> None:
    logger.warning('Update "%s" caused error "%s"', update, error)

def distance(update: Update, context: CallbackContext) -> None:
    dist = 0
    update.message.reply_text("The distance is {}".format(dist))

def list_serial_ports(update: Update, context: CallbackContext) -> None:
    ports = serial.tools.list_ports.comports()
    listOfPorts = ' '.join([str(s) for s in sorted(ports)])
    update.message.reply_text(listOfPorts)

def help(update: Update, context: CallbackContext) -> None:

    update.message.reply_text()

def handler(dispatcher):
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help))
    dispatcher.add_handler(CommandHandler("light_on", turn_on_light,pass_args=True))
    dispatcher.add_handler(CommandHandler("light_off", turn_off_light,pass_args=True))
    dispatcher.add_handler(CommandHandler("change_color", change_colour, pass_args=True))
    dispatcher.add_handler(CommandHandler("invert", invert, pass_args=True))
    dispatcher.add_handler(CommandHandler("ports", list_serial_ports))


def main()-> None:
    
    updater = Updater(token = "2055344400:AAG9-5zc1TSkxfjHLzOs7C4kuV3o9ArO4qw")

    dispatcher = updater.dispatcher
    dispatcher = handler(dispatcher)
    #dispatcher.add_error_handler(error)
    updater.start_polling()

    updater.idle()
    
if __name__ == "__main__":
   
    main()
   