#
#
#
# start|command|value|end
# 03 255 255 255 fc

import serial
from serial.serialutil import SerialException 

START_TOKEN = int(0x03) # 3    #/* Start Frame Token */
END_TOKEN = int(0xFC)   # 252  # /* End Frame Token */

start_hex = b'\x03'
end_hex = b'\xfc'


class UART():
    def __init__(self):
        try:
            self.port = ('COM4')
            self.baudrate = 9600
            self.ser = serial.Serial(            )
            self.ser.baudrate = self.baudrate
            self.ser.port = self.port
            self.end = "\r\n"
            
        except SerialException:
            print(" ConnectionError")
    


    def write(self, data):
        if self.ser.isOpen():
            self.ser.write((data +self.end).encode('ascii'))
        else: 
            try:
                self.ser.open()
                self.ser.write((data +self.end).encode('ascii'))

            except SerialException:
                print("cant open port")

    def readline(self):
        data = self.ser.readline() 
        return data  



