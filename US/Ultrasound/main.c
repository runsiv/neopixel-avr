/*
 * Ultrasound.c
 *
 * Created: 12.10.2021 07:33:52
 * Author : M67376
 */ 
#define F_CPU 4000000
#define echo PIN3_bm
#define trigger PIN4_bm
#define debug_pin PIN2_bm
#define MAX_COMMAND_LEN		8

#include <avr/io.h>
#include "usart.h"
#include <string.h>
#include <util/delay.h>
#include <stdbool.h>
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/cpufunc.h>

uint16_t pulse =0 ;
//static volatile int pulse = 0;
char command[MAX_COMMAND_LEN];
uint8_t index = 0;
static volatile int i = 0;
void interrupt_init(void);
void debug_init(void);
void execute_command(command);

//void us_trigger(void);
//void clock_start(void);
//void trigger_init(void);


void interrupt_init(void)
{
	PORTD.DIR |= echo;
	PORTD.OUT |= echo;
			  
	PORTD.PIN3CTRL |=  PORT_ISC_BOTHEDGES_gc;
	
	
}

void debug_init(void)
{
	PORTF.DIRSET = debug_pin;
	PORTF.OUTSET = debug_pin;
	PORTF.OUTCLR = debug_pin;
}




void execute_command(command)
{
	if(strcmp(command, "ON") == 0)
	{
		//start_led();
		uart_send_string("OK, LED ON.\r\n");
	}
	else if (strcmp(command, "OFF") == 0)
	{
		//stop_led();
		uart_send_string("OK, LED OFF.\r\n");
	}
	else if (strcmp(command, "START") == 0)
	{
		//stop_led();
		uart_send_string("OK, STARTING MEASUREMENT.\r\n");
	}
	else if (strcmp(command, "STOP") == 0)
	{
		//stop_led();
		uart_send_string("OK, STOPPING MEASUREMENT.\r\n");
	}
	else if((strcmp(command, "DISTANCE") == 0))
	{
		//uart_send_int(count_us);
	}
	else
	{
		uart_send_string("Type ON/OFF to control the LED.\r\n");
	}
	
}


int main(void)
{
    /* Replace with your application code */
	ccp_write_io((void*)&(CLKCTRL.OSCHFCTRLA), ( CLKCTRL_FREQSEL_4M_gc)); //24MHZ
	uart_setup();
	
	uart_send_string("hei/r/n");
	interrupt_init();
	trigger_init();
	debug_init();
	sei();
	int16_t count_us = 0;
	TCA0.SINGLE.CTRLA |=TCA_SINGLE_CLKSEL0_bm; //prescaler = 1
    while (1) 
    {
		us_trigger();
		//uart_send_string("hei \r\n");
		if(pulse != 0)
		{
			count_us = pulse / 58;  // pulse_time * speed of sounds (m/s)/2 --> pulsetime /58 for distance in cm
			count_us = count_us/2;
			uart_send_int(count_us);
			uart_send_string("\r\n");
		}
		_delay_ms(500);
		
		
    }
}
//void clock_start(void)
//{
	//TCA0.SINGLE.CTRLA|= TCA_SINGLE_ENABLE_bm; //enable clock
	//TCA0.SINGLE.CTRLA |=TCA_SINGLE_CLKSEL_DIV1_gc;
	//PORTF.OUTSET = debug_pin;
	//
	//
	//
//}

ISR(USART3_RXC_vect)
{

	char c;
	c = uart_read();
	if(c != '\n' && c != '\r')
	{
		command[index++] = c;
		if(index > MAX_COMMAND_LEN)
		{
			index = 0;
		}
	}
	
	if(c == '\r')
	{
		command[index] = '\0';
		index = 0;
		execute_command(command);
	}


}

ISR(PORTD_PORT_vect)
{
	if (i == 0)
	{
		clock_start();
		i = 1;
	}
	else
	{
		TCA0.SINGLE.CTRLA&= ~TCA_SINGLE_ENABLE_bm;
		pulse = TCA0.SINGLE.CNT;
		TCA0_SINGLE_CNT = 0;
	
		
		i = 0;
	}
	PORTD.INTFLAGS = PIN3_bm;
}
