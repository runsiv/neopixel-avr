/*
 * ultrasound.c
 *
 * Created: 13.10.2021 09:25:35
 *  Author: M67376
 */ 

#include "ultrasound.h"


void trigger_init(void)
{
	
	PORTF.DIRSET = trigger;
	PORTF.OUTSET = trigger;
}

void us_trigger(void)
{
	PORTF.OUTSET = trigger;
	_delay_us(10);
	
	PORTF.OUTCLR = trigger;
}

void clock_start(void)
{
	TCA0.SINGLE.CTRLA|= TCA_SINGLE_ENABLE_bm; //enable clock
	TCA0.SINGLE.CTRLA |=TCA_SINGLE_CLKSEL_DIV1_gc;
	
	
	
	
}