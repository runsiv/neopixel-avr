/* 

*/
#include "usart.h"
#include <avr/io.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>



void uart_setup(void)
{
	/*
	PORTB_DIRSET = PIN2_bm;
	USART3_BAUD = (uint16_t)USART3_BAUD_RATE(9600);
	USART3.CTRLB |= USART_TXEN_bm;
	*/
	/*
	set Portb pin 2 as output
	Set baudrate
	Enable tx
	*/
	PORTB.DIRSET = PIN0_bm; //tx
	PORTB.DIRCLR = PIN1_bm; //rx
	USART3_BAUD = (uint16_t)USART3_BAUD_RATE(9600);
	USART3.CTRLB |= USART_TXEN_bm; //enable transmitt
	USART3.CTRLB |= USART_RXEN_bm;
	USART3.CTRLA |= USART_RXCIE_bm; //receive complete interrupt enable
	
	
}


void uart_send_char(char C)
{
	while (!(USART3.STATUS & USART_DREIF_bm))
	{
		;
	}
	USART3.TXDATAL = C;
}

void uart_send_string(const char *str){
	while( *str ) uart_send_char( *str++ );
}

void uart_send_int(uint16_t n)
{
	
	char buffer[8];
	itoa(n, buffer, 10);
	uart_send_string(buffer);
	/*
	uart_send_char(START_TOKEN);
	uart_send_char(n & 0x00FF);
	uart_send_char(n >> 8);
	uart_send_char(END_TOKEN);
	*/
}


char uart_read()
{
	while (!(USART3.STATUS & USART_RXCIF_bm ))
	{
		
	}
	return  USART3.RXDATAL;
	
}

