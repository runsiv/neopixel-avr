/*
 * UART.h
 *
 * Created: 08.10.2021 12:44:46
 *  Author: M67376
 */ 


#ifndef UART_H_
#define UART_H_
#define USART3_BAUD_RATE(BAUD_RATE) ((float)(F_CPU * 64 / (16 * (float)BAUD_RATE)) + 0.5)
#define START_TOKEN 0x03 //start token
#define END_TOKEN 0xFC //end token
#define debug_pin PIN2_bm

#include <avr/io.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>

void uart_setup(void);
void uart_send_char(char C);
void uart_send_string(const char *str);
void uart_send_int(uint16_t n);
char uart_read();
void US_DISABLE(void);



#endif /* UART_H_ */
