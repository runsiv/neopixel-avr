/*
 * AVR uart echo.c
 *
 * Created: 14.10.2021 08:09:51
 * Author : M67376
 */ 
//#define F_CPU 24000000UL 


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <stdlib.h>
#include <util/delay.h>
#include "neopixel_control.h"
#include <avr/cpufunc.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "usart.h"
#include "ultrasound.h"

#define echo PIN3_bm
#define trigger PIN4_bm
#define MAX_COMMAND_LEN		8
char command[MAX_COMMAND_LEN];
uint8_t index_count = 0;
int dist = 0;

static volatile int status_bit = 0;
static volatile int inverted = 0;

void execute_command(char command[]);
uint8_t NUM_LED_REG = 3;
void distance_vs_leds(uint8_t dist);
uint8_t distance = 0;

static volatile int enable_US = 0;


////////////////////// ARRAY Hell
uint8_t tmp_array[24][3]={};

uint8_t off[24][3]=
{
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
	OFF,
};


uint8_t blue[24][3]=
{
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
	BLUE,
};


////////////////////
int main(void)
{
    /* Replace with your application code */
	uart_setup();
	ccp_write_io((void*)&(CLKCTRL.OSCHFCTRLA), ( CLKCTRL_FREQSEL_24M_gc)); //24MHZ
	
	CCL_LOOKUP_TABLE_INIT();
	TIMER_INIT();
	USART_SPI_MODE_INIT();
	EVSYS_INIT();
	
	
	sei();
	
		
	LED_WRITE(off, LED_NUM,LED_COLS);
	RESET();
	uart_send_string("STARTING.. \n\r");
	uint8_t reset_cntr = 0;
    while (1) 
    {
		
		if(enable_US == 1)
		{	
			uint8_t old_val = 0;
			uint8_t new_val = 0;
			uint8_t val = 0;
			US_trigger_init();
			while(enable_US)
			{
				us_trigger();
				new_val = dist;
				if(new_val <=old_val+5 && new_val >= old_val-5 )
				{
					
					val = new_val;
					
					distance_vs_leds(val);
					uart_send_string("Dist ");
					uart_send_int(val );
					uart_send_string("\r\n");
				}
				old_val = new_val;
				_delay_ms(250);
				reset_cntr = 1;
			}
			//long total = 0;
			//uint8_t filter_cnt = 0;
			//uint8_t N = 0;
			//uint8_t max_samples = 3;
			//int average = 0;
			//US_trigger_init();
			//while (enable_US)
			//{
				//us_trigger();
				//
				//
				//total +=dist;
				//if(N >=max_samples)
				//{
					//total -=average;
				//}
				//else
				//{
					//N++;
				//}
				//average = total/N;
				//
				//_delay_ms(500);
				//distance_vs_leds(average);
				//uart_send_string("Dist ");
				//uart_send_int(dist );
				//uart_send_string("\r\n");
				//reset_cntr = 1;
			
			
			
		}
		if (enable_US == 0 && reset_cntr == 1)
		{
			US_DISABLE();
			LED_WRITE(off, LED_NUM, LED_COLS);
			RESET();
			reset_cntr = 0;
		}
		
		
    }
}


//if only there was a switch case dist/10 +0.5
void distance_vs_leds(uint8_t dist)
{
	/// active leds = dist / 10 + 0.5 
	uint8_t tmp_array[LED_NUM][LED_COLS]={};
	memcpy(tmp_array,off, sizeof(off));
	uint8_t active_leds = (dist/10);

	LED_DISTANCE(tmp_array, active_leds,LED_COLS, inverted );
	
}

// UART Commands 
void execute_command(char command[])
{
	if(strcmp(command, "ON") == 0)
	{
		uart_send_string("OK, LED ON.\r\n");
		LED_WRITE(blue,LED_NUM,LED_COLS);
		
	}
	else if (strcmp(command, "OFF") == 0)
	{
		uart_send_string("OK, LED OFF.\r\n");
		LED_WRITE(off, LED_NUM,LED_COLS);
		
	}
	else if (strcmp(command, "GROW") == 0)
	{
		uart_send_string("OK, STARTING GROWING.\r\n");
		GROW(off,LED_NUM,LED_COLS);
		
	}
	else if (strcmp(command, "SHRINK") == 0)
	{
		uart_send_string("OK, STARTING SHRIKING.\r\n");
		SHRINK(blue, LED_NUM, LED_COLS);
		
	}
	else if (strcmp(command, "STOP") == 0)
	{
		uart_send_string("OK, STOPPING MEASUREMENT.\r\n");
		enable_US = 0;
		
	}
	else if((strcmp(command, "DISTANCE") == 0))
	{
		//uart_send_int(count_us);
	}
	else if((strcmp(command, "START") == 0))
	{
		uart_send_string("OK, STARTING MEASUREMENT.\r\n");
		enable_US = 1;
	}
	else if((strcmp(command, "INVERT") == 0))
	{
		uart_send_string("OK, INVERTING.\r\n");
		inverted = !inverted;
	}
	else if((strcmp(command, "SHINE") ==0))
	{
		uart_send_string("OK, SHINE.\r\n");
		LED_BRIGTHER(blue);
		
	}
	else if((strcmp(command, "DIMM") ==0))
	{
		uart_send_string("OK, DIMMING.\r\n");
		LED_DIMM(blue);
		
	}


	else
	{
		uart_send_string("Type ON/OFF to control the LED.\n\r");
	}
	
}

// UART interrupt 
ISR(USART3_RXC_vect)
{

		 char c;
		 c = uart_read();
		
		 if(c != '\n' && c != '\r')
		 {
			 command[index_count++] = c;
			 if(index_count > MAX_COMMAND_LEN)
			 {
				 index_count = 0;
			 }
		 }
		 
		 if(c == '\r')
		 {
			 command[index_count] = '\0';
			 index_count = 0;
			 execute_command(command);
		 }
	 //uart_send_char(c);
	// uart_send_string('\n\r');
}


// US interrrupt
ISR(PORTD_PORT_vect)
{
	uint16_t pulse =0 ;
	if (status_bit == 0)
	{
		clock_start();
		status_bit = 1;
	}
	else
	{
		TCA0.SINGLE.CTRLA&= ~TCA_SINGLE_ENABLE_bm;
		pulse = TCA0.SINGLE.CNT;
		TCA0_SINGLE_CNT = 0;
		dist  = pulse / 58;
		dist = dist/3;
		//distance = distance + dist;
		
		//distance_vs_leds(dist);
		
		
		
		status_bit = 0;
	}
	
	
	PORTD.INTFLAGS = PIN3_bm;
}

