/*
 * ultrasound.h
 *
 * Created: 13.10.2021 09:26:43
 *  Author: M67376
 */ 


#ifndef ULTRASOUND_H_
#define ULTRASOUND_H_

#include <avr/io.h>
#include <util/delay.h>

#define echo PIN3_bm
#define trigger PIN4_bm
#define debug_pin PIN2_bm

void US_trigger_init(void);
void us_trigger(void);
void clock_start(void);




#endif /* ULTRASOUND_H_ */