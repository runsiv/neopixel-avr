/*
* neopixel_controll.c
*
* Created: 20.10.2021 07:49:26
*  Author: M67376
*/
#include "neopixel_control.h"

void EVSYS_INIT(void)
{
	EVSYS.CHANNEL0 = EVSYS_CHANNEL0_USART0_XCK_gc;
	EVSYS.USERTCB1CAPT = EVSYS_USER_CHANNEL0_gc;
	EVSYS.USERTCB2CAPT = EVSYS_USER_CHANNEL0_gc;
}

void USART_SPI_MODE_INIT(void)
{
	USART0.BAUD = (uint16_t)(F_CPU_MHZ * T_WIDTH_NS * 4 / 125 + 32) & 0xFFC0;
	USART0.CTRLC = USART_CMODE_MSPI_gc | USART_UCPHA_bm;
	USART0.CTRLB = USART_TXEN_bm;

}

void TIMER_INIT(void)
{
	//TCB "1" bit generation
	TCB1.CCMP = (uint16_t)(F_CPU_MHZ * T1H_NS / 1000 + 0.5f);
	TCB1.CNT = TCB1.CCMP;
	TCB1.EVCTRL = TCB_CAPTEI_bm;
	TCB1.CTRLB = TCB_CNTMODE_SINGLE_gc;
	TCB1.CTRLA = TCB_ENABLE_bm;
	
	//TCB "0" bit generation
	TCB2.CCMP = (uint16_t)(F_CPU_MHZ * T0H_NS / 1000 + 0.5f);
	TCB2.CNT = TCB2.CCMP;
	TCB2.EVCTRL = TCB_CAPTEI_bm;
	TCB2.CTRLB = TCB_CNTMODE_SINGLE_gc;
	TCB2.CTRLA = TCB_ENABLE_bm;
	
}

void CCL_LOOKUP_TABLE_INIT(void)
{
	CCL.TRUTH0 = 0b11011000;
	CCL.LUT0CTRLC = CCL_INSEL2_TCB2_gc;
	CCL.LUT0CTRLB = CCL_INSEL1_TCB1_gc | CCL_INSEL0_USART0_gc;
	CCL.LUT0CTRLA = CCL_OUTEN_bm | CCL_ENABLE_bm;
	CCL.CTRLA |= CCL_ENABLE_bm;
	PORTA.DIRSET = PIN3_bm;	// CCL-LUT0 OUTPUT
	
}

/////////////////////////////////INIT DONE//////////////////////////////////////////
//void LED_WRITE(uint8_t *ptr)




void LED_WRITE(uint8_t (*led_array)[3], int rows, int cols)
{
	for (int i = 0; i < rows; i++)
	{
		for(int j = 0;j<cols; j++ )
		{
			while(!(USART0.STATUS & USART_DREIF_bm));
			USART0.TXDATAL = led_array[i][j];
			
		}
		
	}
	
	USART0.STATUS = USART_TXCIF_bm;
	while (!(USART0.STATUS & USART_TXCIF_bm));
	
}

void LED_BRIGTHER()
{
	
	
	uint8_t tmp_array[LED_NUM][LED_COLS]={};
	for (uint8_t b = 0x00; b<0xFF; b++)
	{	
		for(int j =0; j<LED_NUM;j++)
		{
			
			tmp_array[j][2] = b;
		}
		
		LED_WRITE(tmp_array, LED_NUM, LED_COLS);
		RESET();
		
		_delay_ms(200);
	}
	
	
}

void LED_DIMM()
{
	
	uint8_t tmp_array[LED_NUM][LED_COLS]={};
	for (uint8_t b = 0xFF; b>=0; b--)
	{
		for(int j =0; j<LED_NUM;j++)
		{
			
			tmp_array[j][2] = b;
		}
		
	LED_WRITE(tmp_array, LED_NUM, LED_COLS);
	RESET();
	
	_delay_ms(200);
	}
}

//LED_DIST asdasdas active leds , rows
void LED_DISTANCE(uint8_t (*LED_ARRAY)[3], int active_leds, int cols, int inverted)
{

	if(inverted == 0)
	{
		LED_WRITE(LED_ARRAY, LED_NUM, cols);
		uint8_t tmp[]= BLUE;
		uint8_t red[] = RED;
		uint8_t off_mode[] = OFF;
		if(active_leds ==0)
		{
			for(uint8_t i = 0; i< 24;i++)
			{
				for(int j =0; j<3;j++)
				{
					LED_ARRAY[i][j] = off_mode[j];
				}
			}
		}
		else
		{
			if (active_leds <4)
			{
				for(uint8_t i = 0; i< 24;i++)
				{
					for(int j =0; j<3;j++)
					{
						if(i <=active_leds)
						{
							LED_ARRAY[i][j] = red[j];
						}
						else
						{
							LED_ARRAY[i][j] = off_mode[j];
						}
						//LED_ARRAY[i][j] = tmp[j];
					}
				}
			}
			else
			{
				for(uint8_t i = 0; i< 24;i++)
				{
					for(int j =0; j<3;j++)
					{
						if(i <=active_leds)
						{
							LED_ARRAY[i][j] = tmp[j];
						}
						else
						{
							LED_ARRAY[i][j] = off_mode[j];
						}
						//LED_ARRAY[i][j] = tmp[j];
					}
				}
			}
		}
	}
	if(inverted ==1)
	{
		
		LED_WRITE(LED_ARRAY, LED_NUM, cols);
		uint8_t tmp[]= BLUE;
		uint8_t off_mode[] = OFF;
		uint8_t red[] = RED;
		if(active_leds<5)
		{
			for(uint8_t i = 24; i> 0;i--)
			{
				
				for(int j =0; j<3;j++)
				{
					if(i >=active_leds)
					{
						LED_ARRAY[i][j] = red[j];
					}
					else
					{
						LED_ARRAY[i][j] = off_mode[j];
					}
					
				}
			}
		}
		else
			{
				for(uint8_t i = 24; i> 0;i--)
				{
					
					for(int j =0; j<3;j++)
					{
						if(i >=active_leds)
						{
							LED_ARRAY[i][j] = tmp[j];
						}
						else
						{
							LED_ARRAY[i][j] = off_mode[j];
						}
						
					}
				}
			}
			
		
	}
	LED_WRITE(LED_ARRAY, LED_NUM, LED_COLS);
	RESET();
}

void SHRINK(uint8_t (*LED_ARRAY)[3], int rows, int cols)
{
	LED_WRITE(LED_ARRAY, 24, 3);
	uint8_t tmp_array[LED_NUM][LED_COLS]={};
	uint8_t tmp_b[]= BLUE;
	//memcpy(tmp_array,LED_ARRAY, sizeof(LED_ARRAY[24][3]));
	
	for(uint8_t i = 0; i<rows; i++)
	{
		for(int j =0; j<cols;j++)
		{
			
			tmp_array[i][j] = tmp_b[j];
		}
		LED_WRITE(tmp_array, rows, cols);
		RESET();
	}
	
	
	uint8_t tmp[]= OFF;
	for(uint8_t i = rows-1; i> 0;i--)
	{
		
		for(int j =0; j<cols;j++)
		{
			tmp_array[i][j] = tmp[j];
		}
		LED_WRITE(tmp_array, rows, cols);
		RESET();
		_delay_ms(500);
	}
	
}

void GROW(uint8_t (*LED_ARRAY)[3], int rows, int cols)
{
	uint8_t tmp_array[LED_NUM][LED_COLS]={};
	memcpy(tmp_array,LED_ARRAY, sizeof(LED_ARRAY[LED_NUM][LED_COLS]));
	
	uint8_t tmp[]= BLUE;

	for(uint8_t i = 0; i<rows; i++)
	{
		for(int j =0; j<cols;j++)
		{
			
			tmp_array[i][j] = tmp[j];
		}
		LED_WRITE(tmp_array, rows, cols);
		RESET();
		_delay_ms(500);
		
	}
	uart_send_string("GROWING FINISHED");
	
}


void RESET(void)
{
	TCB0.CCMP = (uint16_t)(F_CPU * T_RESET_US) - 1;
	TCB0.CNT = 0;
	TCB0.INTFLAGS = TCB_CAPT_bm;
	TCB0.CTRLB = TCB_CNTMODE_INT_gc;
	TCB0.CTRLA = TCB_CLKSEL_DIV1_gc | TCB_ENABLE_bm;
	while (!(TCB0.INTFLAGS & TCB_CAPT_bm));
	TCB0.CTRLA = 0;
}

//void LED_OFF(void)
//{
//uint8_t ptr[24][3]={};
//
//for (int i = 0; i < 24*3; i++)
//{
//while(!(USART0.STATUS & USART_DREIF_bm));
//USART0.TXDATAL = ptr[i];
//
//}
//
//USART0.STATUS = USART_TXCIF_bm;
//while (!(USART0.STATUS & USART_TXCIF_bm));
//}