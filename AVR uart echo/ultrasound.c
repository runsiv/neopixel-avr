/*
 * ultrasound.c
 *
 * Created: 13.10.2021 09:25:35
 *  Author: M67376
 */ 

#include "ultrasound.h"


void US_trigger_init(void)
{
	TCA0.SINGLE.CTRLA |=TCA_SINGLE_CLKSEL0_bm; //prescaler = 1
	PORTD.DIR |= echo;
	PORTD.OUT |= echo;
	
	PORTD.PIN3CTRL |=  PORT_ISC_BOTHEDGES_gc;
	
	PORTF.DIRSET = trigger;
	PORTF.OUTSET = trigger;
}

void us_trigger(void)
{
	
	PORTF.OUTSET = trigger;
	_delay_us(11);
	
	PORTF.OUTCLR = trigger;
}

void clock_start(void)
{
	//need to fix, was original TCA0.SINGLE but that was being used by neopixel driver
	
	TCA0.SINGLE.CTRLA|= TCA_SINGLE_ENABLE_bm; //enable clock
	TCA0.SINGLE.CTRLA |=TCA_SINGLE_CLKSEL_DIV4_gc; //if any changes, div 4 was used
	
	
	
	
}

void US_DISABLE(void)
{
	PORTD.PIN3CTRL |=  PORT_ISC_BOTHEDGES_gc;
}

