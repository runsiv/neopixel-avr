/*
 * neopixel_control.h
 *
 * Created: 20.10.2021 07:49:54
 *  Author: M67376
 */ 


#ifndef NEOPIXEL_CONTROL_H_
#define NEOPIXEL_CONTROL_H_
//#define F_CPU 24000000 //16MHz
#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

// Byte number of RGB data array
#define G	0
#define R	1
#define B	2
#define F_CPU_MHZ	((float)F_CPU / 1000000L)

#define LED_MAX_BRIGHT      0xFF
#define LED_MIN_BRIGHT      0x00

//#define GREEN {0x20, 0x00, 0x00}
#define RED {0x00, 0xFF, 0x00}
#define BLUE  {0x00, 0x00, 0x20}
#define OFF {0x00, 0x00, 0x00}	



// WS2812B parameters
#define T0H_NS		400
#define T1H_NS		800
#define T_WIDTH_NS	1250
#define T_RESET_US	300

#define LED_NUM  24
#define LED_COLS 3
#define LED_NUM_SIZE LED_NUM*3




//FUNCTIONS
void RESET(void);
void LED_WRITE(uint8_t  (*led_array)[3], int rows, int cols);
void CCL_LOOKUP_TABLE_INIT(void);
void TIMER_INIT(void);
void USART_SPI_MODE_INIT(void);
void EVSYS_INIT(void);
void LED_OFF(void);
void LED_OFF(void);
void GROW(uint8_t (*LED_ARRAY)[3], int rows, int cols);
void SHRINK(uint8_t (*LED_ARRAY)[3], int rows, int cols);
void LED_DISTANCE(uint8_t (*LED_ARRAY)[3], int active_leds, int cols, int inverted);
void LED_BRIGTHER();
void LED_DIMM();
#endif /* NEOPIXEL_CONTROL_H_ */